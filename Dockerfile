FROM ubuntu:latest
MAINTAINER Floris Nicolai

RUN apt-get update && apt-get install -y emacs-nox && apt-get clean
RUN mkdir /.emacs.d

WORKDIR /.emacs.d
